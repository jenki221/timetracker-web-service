using DataServicesJSONP;
//------------------------------------------------------------------------------
// <copyright file="WebDataService.svc.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;

namespace WebApplication1
{
    [JSONPSupportBehavior]
    public class WcfDataService1 : DataService< TimeTrackerEntities >
    {
        // This method is called only once to initialize service-wide policies.
        public static void InitializeService(IDataServiceConfiguration config)
        {
            // TODO: set rules to indicate which entity sets and service operations are visible, updatable, etc.

            //temporarily set perms to ALL for testing purposes - mikos j
            config.SetEntitySetAccessRule("*", EntitySetRights.All);

            // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);
            //config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
        }


        //TODO: these methods don't seem to be getting hit - mikos j
        [WebGet]
        public IQueryable<Project> GetProjects()
        {
            var projects = this.CurrentDataSource.Projects.AsQueryable();
            return projects;
        }

        [WebGet]
        public IQueryable<TimeEntry> GetTimeEntries()
        {
            var timeentries = this.CurrentDataSource.TimeEntries.AsQueryable();
            return timeentries;
        }
    }
}