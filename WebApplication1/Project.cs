//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1
{
    using System;
    using System.Collections.Generic;
    
    public partial class Project
    {
        public Project()
        {
            this.Categories = new HashSet<Category>();
        }
    
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public System.DateTime ProjectCreationDate { get; set; }
        public bool ProjectDisabled { get; set; }
        public decimal ProjectEstimateDuration { get; set; }
        public System.DateTime ProjectCompletionDate { get; set; }
        public string ProjectCreatorId { get; set; }
        public string ProjectManagerId { get; set; }
    
        public virtual aspnet_Users aspnet_Users { get; set; }
        public virtual aspnet_Users aspnet_Users1 { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
    }
}
